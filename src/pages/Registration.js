import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Registration() {
    
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [pw1, setPw1] = useState("");
    const [pw2, setPw2] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [isActive, setIsActive] = useState(false);

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if(firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && mobileNo.length >= 11 && pw1 !== "" && pw2 && pw1 === pw2 && username !== "") {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [username, firstName, lastName,email, mobileNo, pw1, pw2]);


    function registerUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if (data === true) {
                
                Swal.fire({
                    title: "Duplicate Email found",
                    icon: "error",
                    text: "Please provide a different email."
                })

            } else{

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify({
                        email: email,
                        username: username,
                        password: pw1,
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    Swal.fire({
                        title: "Successfully registered",
                        icon: "success",
                        text: "Welcome to MYStore, please login to purchase!"
                    })

                    navigate("/login");
                })

                setEmail("");
                setUsername("");
                setPw1("");
                setPw2("");
                setFirstName("");
                setLastName("");
                setMobileNo("");
            }
        })
    };

    return (

        (user.id !== null) ?
        <Navigate to="/wine-gallery"/>
        :
        <div className="row">
            <div className="col form-center">
                <Form className="form" onSubmit = {e => registerUser(e)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value = {email}
                            onChange = {e => setEmail(e.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>
                    <Form.Group controlId="userUsername">
                        <Form.Label>Username</Form.Label>
                        <Form.Control 
                            type="username" 
                            placeholder="Enter username" 
                            value = {username}
                            onChange = {e => setUsername(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="pw1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value = {pw1}
                            onChange = {e => setPw1(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="pw2">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Verify Password" 
                            value = {pw2}
                            onChange = {e => setPw2(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                            type="firstName" 
                            placeholder="Enter First Name" 
                            value = {firstName}
                            onChange = {e => setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                            type="lastName" 
                            placeholder="Enter Last Name" 
                            value = {lastName}
                            onChange = {e => setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="mobileNo">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control 
                            type="mobileNo" 
                            placeholder="Enter Mobile Number" 
                            value = {mobileNo}
                            onChange = {e => setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <br/>
                    {isActive ?
                    <Button variant="success" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                    }
                </Form>
            </div>
        </div>
        
    )

}

