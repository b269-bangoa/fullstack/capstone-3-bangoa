import {Button, Row, Col, Table, Modal, Form} from 'react-bootstrap';
import {NavLink, useParams, Navigate} from 'react-router-dom';
import {useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminDashboard () {

	const [adminProducts, setAdminProducts] = useState([]);
	const {user} = useContext(UserContext);

  const [show, setShow] = useState(false);
  const [editProductId, setEditProductId] = useState('');
  const [editName, setEditName] = useState('');
  const [editDescription, setEditDescription] = useState('');
  const [editPrice, setEditPrice] = useState(0);
  const [editSKU, setEditSKU] = useState('');
  const [editStock, setEditStock] = useState(0);
  const [editIsActive, setEditIsActive] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        });
        const data = await response.json();
        setAdminProducts(data);
      } catch (error) {
        console.error(error);
      }
    };

    const interval = setInterval(() => {
      fetchData();
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  //Handle for closing modal
   const handleClose = () => setShow(false);

   //Handle to set values
   const handleEdit = (adminProduct) => {
       setEditProductId(adminProduct._id);
       setEditName(adminProduct.name);
       setEditDescription(adminProduct.description);
       setEditPrice(adminProduct.price);
       setEditSKU(adminProduct.SKU);
       setEditStock(adminProduct.stock);
       setEditIsActive(adminProduct.isActive);
       setShow(true);
     };

   //Handle for saving updates
   const handleSaveEdit = () => {
   	let updatedProduct = {
   				productId: editProductId,
		      name: editName,
		      description: editDescription,
		      price: editPrice,
		      SKU: editSKU,
		      stock: editStock,
		      isActive: editIsActive
		    }

    fetch(`${process.env.REACT_APP_API_URL}/products/${editProductId}`,{
	  	method: 'PATCH',	
	  	headers: {
	  		'Content-Type': 'application/json',
	  		Authorization: `Bearer ${localStorage.getItem('token')}`
	  	},
	  	body: JSON.stringify(updatedProduct)
	  })
    .then(res => res.json())
    .then(data => {
    	console.log(data)
    		Swal.fire({
	        title: "Successfully updated product",
	        icon: "success"
	    	})
	    	setShow(false);

 		})
  };

  //Handle for archiving product
  const handleArchive = (editProductId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${editProductId}/archive`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            productId: editProductId
        })
    })
    .then(res => res.json())
    .then(data => {

    		setEditIsActive(data.isActive);

        Swal.fire({
	          position: 'top-end',
	          icon: 'success',
	          title: 'Updated status.',
	          showConfirmButton: false,
	          timer: 500
	        })
  	})
};


	return (

		(user.isAdmin !== true) ?
    <Navigate to="/*"/>
    :
    <>
		<div className="text-center mt-4">
			<h2 className="bg-dark text-white">Admin Dashboard</h2>
			<Button variant="success" className="admin-btn" as={NavLink} to={'/admin-add-product'}>Add product</Button>
			<Button variant="success" className="admin-btn" as={NavLink} to={'/admin-user-carts'}>Show User Orders</Button>
		</div>
			<div>
			<h4 className="bg-dark text-white mb-0 mt-3 text-center">All Products</h4>
		</div>
		<table>
			<thead>
			  <tr className="bg-dark text-white text-center">
			    <th>Name</th>
			    <th>Description</th>
			    <th>Price</th>
			    <th>SKU</th>
			    <th>Stock</th>
			    <th>Available</th>
			    <th>Action</th>
			  </tr>
			 </thead>
			 <tbody className="bg-dark text-white my-1">
	        {adminProducts.map((adminProduct) => (
	        		<tr key={adminProduct.id} >
	        		  <td  className="ps-2">{adminProduct.name}</td>
	        		  <td className="ps-2">{adminProduct.description}</td>
	        		  <td className="ps-2">{adminProduct.price}</td>
	        		  <td className="ps-2">{adminProduct.SKU}</td>
	        		  <td className="pe-5 ps-5">{adminProduct.stock}</td>
	        		  <td className="pe-5 ps-5">{(adminProduct.isActive) ? "Yes" : "No"}</td>
	        		  <td variant="dark" className="text-center">
	        		  <>
	        		  <Button className="table-btn bg-light text-dark mb-1 me-1" onClick={()=>handleEdit(adminProduct)}>Edit</Button>
	        		  {
	        		  	(adminProduct.isActive) ? 
	        		  <Button className="table-btn bg-primary text-dark me-1" onClick={() => handleArchive(adminProduct._id)}>Disable</Button>
	        		  	:
	        		  <Button className="table-btn bg-secondary me-1" onClick={() => handleArchive(adminProduct._id)}>Enable</Button>
	        		  }
	        		  </>
	        		  </td>
	        		</tr>
	        ))}
	      </tbody>
	    </table>


       <Modal show={show} onHide={handleClose}>
         <Modal.Header closeButton>
           <Modal.Title>Edit product</Modal.Title>
         </Modal.Header>
         <Modal.Body>
         <Form>
	      		<Form.Group controlId="name">
	              <Form.Label>Name</Form.Label>
	              <Form.Control 
	                  type="name" 
	                  value = {editName}
	                  onChange = {e => setEditName(e.target.value)}
	              />
	          </Form.Group>

	          <Form.Group controlId="description">
	              <Form.Label>Description</Form.Label>
	              <textarea 
	              	className="form-control"
	              	rows="3"
	                  type="description" 
	                  value = {editDescription}
	                  onChange = {e => setEditDescription(e.target.value)}
	              />
	          </Form.Group>

	          <Form.Group controlId="price">
	              <Form.Label>Price</Form.Label>
	              <Form.Control 
	                  type="price" 
	                  value = {editPrice}
	                  onChange = {e => setEditPrice(e.target.value)}
	                  required
	              />
	          </Form.Group>

	          <Form.Group controlId="SKU">
	              <Form.Label>SKU</Form.Label>
	              <Form.Control 
	                  type="SKU" 
	                  value = {editSKU}
	                  onChange = {e => setEditSKU(e.target.value)}
	              />
	          </Form.Group>

	          <Form.Group controlId="stock">
	              <Form.Label>Stock</Form.Label>
	              <Form.Control 
	                  type="stock" 
	                  value = {editStock}
	                  onChange = {e => setEditStock(e.target.value)}
	              />
	          </Form.Group>
      		</Form>
         </Modal.Body>
	         <Modal.Footer>
	           <Button variant="secondary" onClick={handleClose}>
	             Close
	           </Button>
	           <Button variant="primary" type="submit" onClick={handleSaveEdit}>
	             Save Changes
	           </Button>
	         </Modal.Footer>
       </Modal>
   </>
	)
};