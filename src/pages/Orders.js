import { Button, Row, Col, Table, Modal, Form } from 'react-bootstrap';
import { NavLink, useParams, Navigate } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Orders() {
  const { user } = useContext(UserContext);
  const { userId } = useParams();

  const [orders, setOrders] = useState([]);
  const [activeOrders, setActiveOrders] = useState([]);
  const [inactiveOrders, setInactiveOrders] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/orders/${userId}/orders`,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
          }
        );
        const data = await response.json();
        setOrders(data);
        setActiveOrders(data.filter((order) => order.isActive));
        setInactiveOrders(data.filter((order) => !order.isActive));
      } catch (error) {
        console.error(error);
      }
    };

    //Refactored to fetch changes every second
    const interval = setInterval(() => {
      fetchData();
    }, 1000);

    return () => clearInterval(interval);
  }, [userId]);

  return (

    user.isAdmin === true ? 
    <Navigate to="/*" />
   : 
    <>
      <div className="text-center mt-4">
        <h2 className="bg-dark text-white">My Orders</h2>
      </div>
      <table className="mt-2 w-100">
        <thead>
          <tr className="bg-dark text-white">
            <th>Product Id</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody className="bg-dark text-white">
          {activeOrders.map((order) => (
            <tr key={order.id}>
              <td className="pe-4 ps-4">{order.productId}</td>
              <td className="pe-4 ps-4">{order.quantity}</td>
              <td className="pe-4 ps-4">{order.total}</td>
              <td className="pe-4 ps-4">Yes</td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="text-center mt-4">
        <h2 className="bg-dark text-white">Order History</h2>
      </div>
      <table className="mt-2 w-100">
        <thead>
          <tr className="bg-dark text-white">
            <th>Product Id</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody className="bg-dark text-white">
          {inactiveOrders.map((order) => (
            <tr key={order.id}>
              <td className="pe-4 ps-4">{order.productId}</td>
              <td className="pe-4 ps-4">{order.quantity}</td>
              <td className="pe-4 ps-4">{order.total}</td>
              <td className="pe-4 ps-4">Completed</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}
