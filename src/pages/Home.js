import Banners from '../components/Banners';

export default function Home () {

	const data = {
		title: "Main Page Underconstruction",
		content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eleifend, nibh ac pellentesque iaculis, eros neque consectetur magna, id cursus leo leo ac nisl. Donec vitae velit in nibh convallis feugiat. Etiam at lectus massa. Maecenas tristique rhoncus justo, et tempus ex cursus sit amet. Maecenas lacinia pretium felis, vel varius dui. Sed convallis nunc vitae scelerisque vehicula. Praesent tristique eget ante id facilisis. Mauris ullamcorper sem sit amet sollicitudin sodales. Donec justo tellus, accumsan at scelerisque quis, ultrices quis justo. Etiam in pellentesque elit, a pharetra massa. Cras mollis eros sit amet consectetur vestibulum. Ut non nulla a quam consectetur pharetra vel a mi. Quisque pulvinar orci et eros luctus efficitur. Praesent hendrerit condimentum lacus, at interdum nunc. Sed et augue at nulla auctor ultrices ut ut metus. Morbi tincidunt ante rhoncus nulla tempus ullamcorper.Curabitur sollicitudin diam nec bibendum cursus. Sed nulla dolor, fermentum in suscipit id, interdum egestas justo. Donec magna purus, suscipit vel pulvinar ut, faucibus nec tellus. Integer mollis diam vel ipsum ultrices, ut egestas urna finibus. Morbi non arcu in sapien bibendum porttitor. Suspendisse justo mauris, mollis non dolor at, cursus ornare nisi. Suspendisse fermentum, sapien quis feugiat efficitur, sapien ligula porta libero, id tempus arcu diam nec massa.",
		destination: "/wine-gallery",
		label: "Check Wine Gallery"
	}

	return (
		<Banners data={data}/>
		
		
	)
}