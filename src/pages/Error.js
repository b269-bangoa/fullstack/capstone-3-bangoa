import Banners from '../components/Banners';

export default function Error () {

	const data = {
		title: "Error 404 - Page not found",
		content: "The page you're looking for is not found.",
		destination: "/",
		label: "Back to home"
	}

	return (
		<Banners data={data}/>
		)
};