import {useState, useEffect, useContext} from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import {NavLink, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Products() {

	const [products, setProducts] = useState([]);
	const {user} = useContext(UserContext);
	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`,{
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					product
				)
			}))
		})
	}, []);

	return (
	
		(user.isAdmin) ?
		<Navigate to="/*"/>
		:
		<>
		<div className="container-fluid d-inline">
	      <Row className="justify-content-center mt-2">
	        {products.map((card) => 
	          <Col xs={12} md={3} className="card py-2">
	            <div key={card.id}>
	              <h5>{card.name}</h5>
	              <p>Description: {card.description}</p>
	              <p>Php: {card.price}</p>
	              <Button className="bg-primary card-btn" as={NavLink} to={`/wine-gallery/${card._id}`}>Details</Button>
	            </div>
	          </Col>
	          )}
	      </Row>
	    </div>
		</>
	
		
	)
};