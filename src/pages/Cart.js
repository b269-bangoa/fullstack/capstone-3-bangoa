import React, { useEffect, useState, useContext } from 'react';
import Swal from 'sweetalert2';
import { useNavigate, useParams } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Cart () {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const {userId} = useParams();
  const [cart, setCart] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchCart = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/users/cart/${userId}`,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
          }
        );
        const data = await response.json();
        setCart(data);
        console.log(cart)
        setLoading(true);
      } catch (error) {
        console.error(error);
      }
    };
    fetchCart();
  }, [user.id]);

  return (
    <Container>
      <Row>
        <Col>
          <div className="bg-dark text-white">
            <h2>My Cart</h2>
            {loading ? (
              <p>Loading cart...</p>
            ) : (
              <>
                <table>
                  <thead>
                    <tr>
                      <th className="pe-5 ps-5">Total: Php {cart.total}</th>
                      <th className="pe-5 ps-5">
                        Purchase Date {cart.purchaseDate}
                      </th>
                    </tr>
                    <tr>
                      <th>Product Id</th>
                      <th>Quantity</th>
                      <th>Subtotal</th>
                    </tr>
                  </thead>
                  <tbody>
                    {cart.map((product) => {
                      return (
                        <tr key={product.id}>
                          <td className="pe-5 ps-5">{product.id}</td>
                          <td className="pe-5 ps-5">{product.quantity}</td>
                          <td className="pe-5 ps-5">{product.subTotal}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </>
            )}
          </div>
        </Col>
      </Row>
    </Container>
  );
}
