import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(){

    const {user, setUser} = useContext(UserContext);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    function authenticate(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                username: username,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);

            retrieveUserDetails(data.access);
            
             Swal.fire({
                   title: "Login Successful",
                   icon: "success",
                   text: "Welcome to MYStore!"
               });

             navigate("/home")

            } else {
             Swal.fire({
                   title: "Authentication Failed",
                   icon: "error",
                   text: "Please, check your login details and try again."
               })
                
            }
            
        })

        setUsername('')
        setPassword('')
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((username !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [username, password])

    return(

        (user.id !== null) ?
        <Navigate to="/wine-gallery"/>
        :
        <div className="row form-center">
        	<div className="col-md-6">
        		<Form className="form" onSubmit={e => authenticate(e)}>
        		    <Form.Group controlId="userUsername">
        		        <Form.Label>Username</Form.Label>
        		        <Form.Control 
        		            type="username" 
        		            placeholder="Enter valid username"
        		            value={username}
        		            onChange={e => setUsername(e.target.value)}
        		            required
        		        />
        		    </Form.Group>

        		    <Form.Group controlId="password">
        		        <Form.Label>Password</Form.Label>
        		        <Form.Control 
        		            type="password" 
        		            placeholder="Password"
        		            value={password}
        		            onChange={e => setPassword(e.target.value)}
        		            required
        		        />
        		    </Form.Group>
        		    <br/>
        		    {   isActive ?
        		        <Button variant="success" type="submit" id="submitBtn">
        		            Submit
        		        </Button>
        		        :
        		        <Button variant="danger" type="submit" id="submitBtn" disabled>
        		            Submit
        		        </Button>
        		    }
        		</Form>
        	</div>
        </div>
    )
};