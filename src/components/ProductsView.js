import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CloseButton , Modal} from 'react-bootstrap';
import {useParams, useNavigate, Link, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductsView ({product}) {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [SKU, setSKU] = useState("");
	const [stock, setStock] = useState(0);
	const [show, setShow] = useState(false);
	const [quantity, setQuantity] = useState(1);
	const [cartMessage, setCartMessage] = useState("");

	useEffect(() => {
	  const interval = setInterval(() => {
	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
	      .then(res => res.json())
	      .then(data => {
	        setName(data.name);
	        setDescription(data.description);
	        setPrice(data.price);
	        setSKU(data.SKU);
	        setStock(data.stock);
	      })
	  }, 1000);
	  
	  return () => clearInterval(interval);
	}, [productId]);

   const handleClose = () => setShow(false);

	const handleAddToCart = () => {
	  setShow(true);
	  if (quantity > 0 && quantity <= stock) {
	    fetch(`${process.env.REACT_APP_API_URL}/${user.id}/cart/add`, {
	      method: "POST",
	      headers: { 
	        'Content-Type': 'application/json', 
	        Authorization: `Bearer ${localStorage.getItem('token')}`
	      },
	      body: JSON.stringify({
	        productId: productId,
	        quantity: quantity
	      })
	    })
	    .then(res => res.json())
	    .then(data => {
	      if(data.quantity > stock || data.quantity < 0) {
	        Swal.fire({
	          position: 'top-end',
	          icon: 'error',
	          title: 'Not enough stock',
	          showConfirmButton: false,
	          timer: 1000
	        })
	      } else {
	        // Deduct the quantity from the product stock
	        const newStock = stock - quantity;
	        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
	          method: "PATCH",
	          headers: {
	            "Content-Type": "application/json",
	            Authorization: `Bearer ${localStorage.getItem("token")}`,
	          },
	          body: JSON.stringify({
	            stock: newStock,
	          }),
	        })
	        .then(() => {
	          setStock(newStock);
	        })
	        .catch((error) => {
	          console.error(error);
	        });

	        Swal.fire({
	          position: 'top-end',
	          icon: 'success',
	          title: 'Item/s added to your cart',
	          showConfirmButton: false,
	          timer: 1000
	        })
	        setShow(false)
	      }
	    })
	    .catch(error => {
	      console.error(error);
	    });
	  } else {
	    Swal.fire({
	      position: 'top-end',
	      icon: 'error',
	      title: 'Invalid quantity',
	      showConfirmButton: false,
	      timer: 1000
	    })
	  }
	};


	const handleCartSubmit = (e) => {
	  e.preventDefault();

	  fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/cart/add`, {
	    method: "POST",
	    headers: {
	      "Content-Type": "application/json",
	      Authorization: `Bearer ${localStorage.getItem("token")}`,
	    },
	    body: JSON.stringify({
	      productId: productId,
	      quantity: quantity,
	    }),
	  })
	    .then((res) => res.json())
	    .then((data) => {
	      if (data.quantity > stock || data.quantity < 0) {
	        return false;
	      } else {
	        setShow(false);
	        Swal.fire({
	          title: "Item added to cart",
	          text: "Successfully updated cart",
	          icon: "success",
	        });
	      }
	    })
	    .catch((error) => {
	      console.error(error);
	    });
	};

	const handleCheckOut = () => {
	  const total = price * quantity;

	  const newOrder = {
	  	userId: user.id,
	    productId: productId,
	    quantity: quantity,
	    total: total,
	  };

	  fetch(`${process.env.REACT_APP_API_URL}/orders/add`, {
	    method: "POST",
	    headers: {
	      "Content-Type": "application/json",
	      Authorization: `Bearer ${localStorage.getItem("token")}`,
	    },
	    body: JSON.stringify(newOrder),
	  })
	    .then((res) => res.json())
	    .then((data) => {
	      if (data.quantity > stock || data.quantity < 0) {
	        Swal.fire({
	          position: 'top-end',
	          icon: 'error',
	          title: 'Invalid quantity',
	          showConfirmButton: false,
	          timer: 1000
	        })
	      } else {
	        setShow(false);
	        Swal.fire({
	          title: "Order placed!",
	          text: "Successfully placed order",
	          icon: "success",
	        });
	      }
	    })
	    .catch((error) => {
	      console.error(error);
	    });
	};


	return (

		
			(user.isAdmin) ?
			<Navigate to="/*"/>
			:
			<>		
			<Container>
				<Row>
					<Col lg={{span: 6, offset:3}} >
						<Card>
						  <button className="btn-close bg-secondary" color="white" onClick={() => navigate('/wine-gallery')}></button>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>SKU</Card.Subtitle>
					        <Card.Text>{SKU}</Card.Text>
					        {
					        	(user.id !== null) ?
					        	<Button variant="primary" onClick={handleAddToCart}>Add to cart</Button>
					        	:
					        	<Button variant="primary" as={Link} to="/login">Sign in</Button>
					        }
					      </Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>

	       <Modal show={show} onHide={handleClose}>
	         <Modal.Header closeButton>
	           <Modal.Title>Add to Cart</Modal.Title>
	         </Modal.Header>
	         <Modal.Body>
	         	<p>Name: {name}</p>
	         	<p>Price: {price}</p>
	         	<form onSubmit={handleClose}>
		            <label className="me-2">Quantity:</label>
		            <input 
		            	type="number" 
		            	id="quantity" 
		            	name="quantity" 
		            	value={quantity}
		            	onChange = {e => setQuantity(e.target.value)}
		            />
		          </form>
	         </Modal.Body>
		         <Modal.Footer>
		           <Button variant="primary" onClick={handleCartSubmit}>
		             Add
		           </Button>
		           <Button variant="success" onClick={handleCheckOut}>
		             Place Order
		           </Button>
		         </Modal.Footer>
	       </Modal>
			</>
		
	)
}
