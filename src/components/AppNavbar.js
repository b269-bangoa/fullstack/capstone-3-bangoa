import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {Link, NavLink} from 'react-router-dom';

import {useState, useContext} from 'react';
import UserContext from '../UserContext';
import Image from 'react-bootstrap/Image';

export default function AppNavbar() {

 const {user} = useContext(UserContext);

  return (
    <>
    <Navbar className="navbar py-0">
      <Container fluid className="d-flex">
        <Navbar.Brand as={Link} to="/">MYStore</Navbar.Brand>

          {
            (user.isAdmin)  ?
            <Nav
              className="me-auto my-0"
              style={{ maxHeight: '100px' }}
            >
              <Nav.Link as={NavLink} to="/admin-dashboard">Admin Dashboard</Nav.Link>
              <Nav.Link as={NavLink} to="/admin-users">Users</Nav.Link>
            </Nav>
            :
            <>
            <Nav
              className="me-auto my-0"
              style={{ maxHeight: '100px' }}
            >
              <Nav.Link as={NavLink} to="/wine-gallery">Wine Gallery</Nav.Link>
            </Nav>
            </>
          }
            
          <Nav
            className="ms-auto me-2 text-nav"
            style={{ maxHeight: '100px' }} >
            {
              (user.id !== null) ?
              <>
              {
                (user.isAdmin) ? 
                <Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>
                :
                <>
                <Nav.Link as={NavLink} to="/:userId/cart">Cart</Nav.Link> 
                <Nav.Link as={NavLink} to="/:userId/orders">Orders</Nav.Link> 
                <Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>
                </>
              }
              </>
              :
              <>
              <Nav.Link as={NavLink} to="/login" >Sign in</Nav.Link>
              <Nav.Link as={NavLink} to="/register" >Register</Nav.Link>
              </>
            }
          </Nav>
      </Container>
    </Navbar>
    
    </>
  );
};