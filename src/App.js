import './App.css';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Registration from './pages/Registration';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Products from './pages/Products';
import Cart from './pages/Cart';
import Orders from './pages/Orders';
import ProductsView from './components/ProductsView';
import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './admin/CreateProduct';
import AdminUsersView from './admin/AdminUsersView';
import UserOrders from './admin/UserOrders';

import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext';

export default function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        }) 
      } else {
          setUser({
            id: null,
            isAdmin: null
          })
      }
    })
  }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/home" element={<Home/>}/>
              <Route path="/register" element={<Registration/>}/>
              <Route path="/wine-gallery" element={<Products/>}/>
              <Route path="/wine-gallery/:productId" element={<ProductsView/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="/:userId/cart" element={<Cart/>}/>
              <Route path="/:userId/orders" element={<Orders/>}/>
              <Route path="/*" element={<Error/>}/>
              {/*Admin paths*/}
              <Route path="/admin-dashboard" element={<AdminDashboard/>}/>
              <Route path="/admin-add-product" element={<CreateProduct/>}/>
              <Route path="/admin-users" element={<AdminUsersView/>}/>
              <Route path="/admin-user-carts" element={<UserOrders/>}/>
            </Routes>
            </Container> 
        </Router>
      </UserProvider>
    </>
    
  );
};