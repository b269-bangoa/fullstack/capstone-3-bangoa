import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import {NavLink} from 'react-router-dom';
import {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {Edit} from 'react-admin';
import Swal from 'sweetalert2';

export default function AdminProductView () {


  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [isAdmin, setIsAdmin] = useState(false);
  const [users, setUsers] = useState([]);
  const [userId, setUserId] = useState();

  useEffect(() => {
    const fetchData = () => {
      fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {
          setUsers(data)
        })
        .catch(error => {
          console.error(error)
        })
    }

    fetchData()

    const interval = setInterval(() => {
      fetchData()
    }, 1000)

    return () => clearInterval(interval)
  }, [])


  const handleEdit = (user) => {
    setUserId(user._id)
    setIsAdmin(user.isAdmin)
  }

  const handleSetAsAdmin = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/set-as-admin`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            userId: userId
        })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      setIsAdmin(data.isAdmin);

      Swal.fire({
          position: 'center',
          icon: 'info',
          title: 'Warning! You just updated this user\' role in this page.',
          showConfirmButton: true
        })
    })
  };

  return (
    <table className="mt-2 w-100">
    <thead>
      <tr className="bg-dark text-white">
        <th>Email</th>
        <th>Username</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Mobile No.</th>
        <th className="text-center">Admin</th>
        <th className="text-center">Action</th>
      </tr>
      </thead>
      <tbody className="bg-dark text-white my-1">
        {users.map((user) => (
          <tr key={user._id}>
            <td>{user.email}</td>
            <td>{user.username}</td>
            <td>{user.firstName}</td>
            <td>{user.lastName}</td>
            <td>{user.mobileNo}</td>
            <td className="text-center">{(user.isAdmin) ? "Yes" : "No"}</td>
            <td variant="dark" className="text-center">
            {
              (user.isAdmin) ?
              <Button className="table-btn bg-danger me-1 text-center my-2" onClick={()=>handleSetAsAdmin(user._id)} >Remove</Button>
              :
              <Button className="table-btn bg-secondary me-1 text-center my-2" onClick={()=>handleSetAsAdmin(user._id)} >Enable</Button>
            }
            </td>
          </tr>
        ))}
         </tbody>
      </table>

      
  );
};
