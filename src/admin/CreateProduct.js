import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CreateProduct ({product}) {

	/*const {name, description, price, SKU, stock} = product;*/

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [SKU, setSKU] = useState("");
	const [stock, setStock] = useState(0);
	const [isActive, setIsActive] = useState(false);

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {
	    if(name !== "" && description !== "" && price > 0 && SKU !== "" && stock > 0) {
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }
	}, [name, description, price, SKU, stock]);

	function addProduct (e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/checkProduct`, {
		    method: "POST",
		    headers: {'Content-Type': 'application/json'},
		    body: JSON.stringify({
		        name: name
		    })
		})
		.then(res => res.json())
		.then(data =>{

		    if (data === true) {
		        
		        Swal.fire({
		            title: "Duplicate Product found",
		            icon: "error",
		            text: "Product already registered. Please check again"
		        })

		    } else{
				fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
		            method: "POST",
		            headers: {
		            	'Content-Type': 'application/json',
		            	Authorization: `Bearer ${localStorage.getItem('token')}`
		            },
		            body: JSON.stringify({
		                name: name,
		                description: description,
		                price: price,
		                SKU: SKU,
		                stock: stock
		            })
		        })
		    	.then(data => {

		    		Swal.fire({
		    			title: "Successfully added a product.",
		    			icon: "success",
		    			text: "Please recheck product details registered."
		    		})

		    		navigate('/admin-dashboard')

		    	})
		    }

		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSKU('');
		    setStock(0);
		})
	};

	return (

		<div className="row">
		    <div className="col d-flex justify-content-center">
		        <Form className="form-add-product" onSubmit = {e => addProduct(e)}>
		            <Form.Group controlId="name">
		                <Form.Label>Name</Form.Label>
		                <Form.Control 
		                    type="name" 
		                    placeholder="Enter product name" 
		                    value = {name}
		                    onChange = {e => setName(e.target.value)}
		                    required
		                />
		            </Form.Group>

		            <Form.Group controlId="description">
		                <Form.Label>Description</Form.Label>
		                <textarea 
		                	class="form-control"
		                	rows="3"
		                    type="description" 
		                    placeholder="Enter product description" 
		                    value = {description}
		                    onChange = {e => setDescription(e.target.value)}
		                    required
		                />
		            </Form.Group>

		            <Form.Group controlId="price">
		                <Form.Label>Price</Form.Label>
		                <Form.Control 
		                    type="price" 
		                    placeholder="Enter product price" 
		                    value = {price}
		                    onChange = {e => setPrice(e.target.value)}
		                    required
		                />
		            </Form.Group>

		            <Form.Group controlId="SKU">
		                <Form.Label>SKU</Form.Label>
		                <Form.Control 
		                    type="SKU" 
		                    placeholder="Enter product sku" 
		                    value = {SKU}
		                    onChange = {e => setSKU(e.target.value)}
		                    required
		                />
		            </Form.Group>

		            <Form.Group controlId="stock">
		                <Form.Label>Stock</Form.Label>
		                <Form.Control 
		                    type="stock" 
		                    placeholder="Enter stock quantity" 
		                    value = {stock}
		                    onChange = {e => setStock(e.target.value)}
		                    required
		                />
		            </Form.Group>

		            <br/>
		            {isActive ?
		            <Button variant="success" type="submit" id="submitBtn">
		                Submit
		            </Button>
		            :
		            <Button variant="danger" type="submit" id="submitBtn" disabled>
		                Submit
		            </Button>
		            }
		        </Form>
		    </div>
		</div>
	)
}