import {Button, Row, Col, Table, Modal, Form} from 'react-bootstrap';
import {NavLink, useParams, Navigate} from 'react-router-dom';
import {useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function UserOrders () {

	const [orders, setOrders] = useState([]);
	const [orderId, setOrderId] = useState('');
	const [products, setProducts] = useState([]);
	const [total, setTotal] = useState(0);
	const [isActive, setIsActive] = useState(true);
	const [editOrderIsActive, setEditOrderIsActive] = useState(true);
	const {user} = useContext(UserContext);

  const [show, setShow] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        });
        const data = await response.json();
        setOrders(data);

      } catch (error) {
        console.error(error);
      }
    };

    //Refactored to fetch changes every second
    const interval = setInterval(() => {
      fetchData();
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  const handleEdit = (order) => {
  	setOrderId(order._id)
  	setIsActive(order.isActive)
  }

  const handleStatus = (orderId) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/status`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            orderId: orderId
        })
    })
    .then(res => res.json())
    .then(data => {
        const updatedOrders = orders.map(order => {
            if (order._id === orderId) {
                return {
                    ...order,
                    isActive: data.isActive
                };
            } else {
                return order;
            }
        });
        setOrders(updatedOrders);

        Swal.fire({
            title: "Updated Status",
            text: "Successfully updated status of product",
            icon: "success",
            showConfirmButton: false,
            timer: 1000
        });
    })
    .catch(err => {
        console.error(err);
    });
  };



	return (

		(user.isAdmin !== true) ?
    <Navigate to="/*"/>
    :
    <>
		<div className="text-center mt-4">
			<h2 className="bg-dark text-white">All User Orders</h2>
		</div>
		<table className="mt-2 w-100">
			<thead>
			  <tr className="bg-dark text-white text-center">
			    <th>UserId</th>
			    <th>Product Id</th>
			    <th>Quantity</th>
			    <th>Total</th>
			    <th>Active</th>
			    <th>Action</th>
			  </tr>
			 </thead>
			 <tbody className="bg-dark text-white">
	        {orders.map((order) => (
	        		<tr key={order.id} >
	        		  <td className="pe-4 ps-4" >{order.userId}</td>
	        		  <td className="pe-4 ps-4">{order.productId}</td>
	        		  <td className="pe-4 ps-4">{order.quantity}</td>
	        		  <td className="pe-4 ps-4">{order.total}</td>
	        		  <td className="pe-4 ps-4">{(order.isActive) ? "Yes" : "No"}</td>
	        		  <td variant="dark" className="text-center ps-4 pe-4 py-2">
	        		  {
                        (order.isActive) ?
                        <Button className="table-btn bg-primary me-1" onClick={()=>handleStatus(order._id)}>To ship</Button>
                        :
                        <Button className="table-btn bg-secondary me-1" onClick={()=>handleStatus(order._id)}>Shipped</Button>
                      }
	        		  </td>
	        		</tr>
	        ))}
	      </tbody>
	    </table>
   </>
	)
};
