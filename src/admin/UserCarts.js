import React, { useEffect, useState, useContext } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function UserCarts() {
  const navigate = useNavigate();

  const [carts, setCarts] = useState([]);
  const {user} = useContext(UserContext);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/show?isAdmin=false`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCarts(data);

        if (data === false) {
          navigate('/error');
        }
      });
  }, [navigate]);

  return (
    <Container>
      <Row>
        <Col>
          <div className="bg-dark text-white">
            <h2 className="text-center">User Orders</h2>
            <table className="mt-2">
              <thead>
                <tr>
                  <th className="pe-5 ps-5">UserId</th>
                  <th className="pe-5 ps-5">Total</th>
                  <th className="pe-5 ps-5">Active</th>
                  <th className="pe-5 ps-5">Purchase Date</th>
                </tr>
              </thead>
              <tbody>
                {carts &&
                  carts.map((cart) => (
                    <tr key={cart.id}>
                      <td className="pe-5 ps-5">{cart.userId}</td>
                      <td className="pe-5 ps-5">{cart.total}</td>
                      <td className="pe-5 ps-5">{cart.isActive}</td>
                      <td className="pe-5 ps-5">{cart.purchaseDate}</td>
                    </tr>
                  ))}
                {!carts && (
                  <tr>
                    <td colSpan="4">
                      <h3>Nothing to see here</h3>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </Col>
      </Row>
    </Container>
    </>
  }
    
  );
}
